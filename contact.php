<?php
/**
 * @package WordPress
 * @subpackage Textura
 Template Name: Contact Template
 Description: Contact Form
*/
?>

<?php
if($_POST[sent]) {
	$error = "";

	$fname = strip_tags(trim($_POST[form_name]));
	$sender = strip_tags(trim($_POST[form_email]));
	$message = htmlspecialchars(trim($_POST[form_message]));

	if(!$fname) {
		$error .= __( '<span>Please enter your Name.</span><br />', 'textura');
	}
	if(!filter_var($sender,FILTER_VALIDATE_EMAIL)) {
		$error .=__( '<span>Please enter a valid e-mail address.</span><br />', 'textura');
	}
	if(!$message) {
		$error .= __( '<span>Please enter a message.</span><br />', 'textura');
	}
	if(!$error) {
		$recipient = get_option("admin_email");
		$subject = get_option("blogname")." - Webmailer";
		$body = $message;
		$header = "From: ".$fname.
			"<".$sender.">\r\nReply-To:".$sender. "\r\n" .
			"MIME-Version: 1.0" . "\r\n" .
			"Content-type: text/html; charset=UTF-8" . "\r\n";

		$email = mail($recipient, $subject, $body, $header);
	}
}
?>

<?php get_header(); ?>
<section id="content">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
			<header class="post-header">
				<h1 class="post-title"><?php the_title(); ?></h1>
			</header><!-- .post-header -->
		<?php if ($email) : ?>
			<h1><?php _e( 'Message was sent successfully!', 'textura'); ?></h1>
			<p><?php _e( 'We will answer you as soon as possible.', 'textura'); ?></p>
		<?php else : if ($error) { ?>
			<h1><?php _e( 'Validation Error!', 'textura'); ?></h1>
			<?php echo $error; ?>
		<?php } else the_content(); ?>
			<form action="<?php the_permalink(); ?>" id="contactform" method="post">
				<p><input type="hidden" name="sent" id="sent" value="1" />
				<table id="contact">
					<tr>
						<td>
							<label for="form_name" id="name"><?php _e( 'Name', 'textura' ); ?></label><br />
							<input type="text" name="form_name" id="name" value="<?php print $fname; ?>" placeholder="Your Name" autofocus />
						</td>
					</tr>
					<tr>
						<td>
							<label for="form_email" id="email"><?php _e( 'Email', 'textura' ); ?></label><br />
							<input type="email" name="form_email" id="email" value="<?php print $sender; ?>" placeholder="name@domain.tld" />
						</td>
					</tr>
					<tr>
						<td>	
							<label for="form_message" id="message"><?php _e( 'Message', 'textura' ); ?></label><br />
							<textarea name="form_message" id="message" rows="10" cols="40"><?php print $message; ?></textarea>
						</td>
					</tr>
					<tr>
						<td>	
							<input class="submit" type="submit" name="send" value="<?php _e( 'Send', 'textura' ); ?>" />
						</td>
					</tr>
				</table><!-- #contact -->
				</form><!-- #contactform -->
			<?php edit_post_link( __( 'Edit', 'textura' ), '<span class="edit-link">', '</span>' ); ?>
			</article><!-- #post-<?php the_ID(); ?>-->
				<?php endif; ?>
		<?php endwhile; ?>
</section><!-- #content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>