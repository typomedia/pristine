<?php
/**
 * @package WordPress
 * @subpackage Pristine
 */
?>

<?php get_header(); ?>

	<section id="content">

			<?php the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
				<header class="post-header">
					<h1 class="post-title"><?php the_title(); ?></h1>
				</header><!-- .post-header -->

				<div class="entry-content">
					<?php the_content(); ?>
					<?php wp_link_pages( array( 'before' => '<span class="page-link">' . __( 'Pages:', 'pristine' ), 'after' => '</span>' ) ); ?>
					<?php edit_post_link( __( 'Edit', 'pristine' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- .post-content -->
			</article><!-- #post-<?php the_ID(); ?> -->

			<?php comments_template( '', true ); ?>

	</section><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>