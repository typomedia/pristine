<?php
/**
 * @package WordPress
 * @subpackage Pristine
 */
?>

<?php get_header(); ?>

	<section id="content">

		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>

				<div class="entry-meta">
					<?php
						printf( __( '<span class="sep">Posted on </span><a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="%4$s" title="%5$s">%6$s</a></span>', 'pristine' ),
							get_permalink(),
							get_the_date( 'c' ),
							get_the_date(),
							get_author_posts_url( get_the_author_meta( 'ID' ) ),
							sprintf( esc_attr__( 'View all posts by %s', 'pristine' ), get_the_author() ),
							get_the_author()
						);
					?>
				</div><!-- .entry-meta -->
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'pristine' ), 'after' => '</div>' ) ); ?>
			</div><!-- .entry-content -->

			<footer class="entry-meta">
				<?php
					$tag_list = get_the_tag_list( '', ', ' );
					if ( '' != $tag_list ) {
						$utility_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'pristine' );
					} else {
						$utility_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'pristine' );
					}
					printf(
						$utility_text,
						get_the_category_list( ', ' ),
						$tag_list,
						get_permalink(),
						the_title_attribute( 'echo=0' )
					);
				?>

				<?php edit_post_link( __( 'Edit', 'pristine' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-meta -->
		</article><!-- #post-<?php the_ID(); ?> -->
	
			<nav id="nav-below" role="navigation">
				<h1 class="section-heading"><?php _e( 'Post navigation', 'pristine' ); ?></h1>
				<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&laquo;', 'Previous post link', 'pristine' ) . '</span> %title' ); ?></div>
				<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&raquo;', 'Next post link', 'pristine' ) . '</span>' ); ?></div>
			</nav><!-- #nav-below -->

			<?php comments_template( '', true ); ?>

		<?php endwhile; ?>

	</section><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>