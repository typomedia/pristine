<?php
/**
 * @package WordPress
 * @subpackage Pristine
 */
 ?>

<?php get_header(); ?>

<section id="content">

	<?php the_post(); ?>

	<?php rewind_posts(); ?>

	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav id="nav-above" role="navigation">
			<h1 class="section-heading"><?php _e( 'Post navigation', 'pristine' ); ?></h1>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'pristine' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'pristine' ) ); ?></div>
		</nav><!-- #nav-above -->
	<?php endif; ?>
				
	<?php while ( have_posts() ) : the_post(); ?>
				
		<?php get_template_part( 'content', get_post_format() ); ?>

	<?php endwhile; ?>
				
	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav id="nav-below" role="navigation">
			<h1 class="section-heading"><?php _e( 'Post navigation', 'pristine' ); ?></h1>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'pristine' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'pristine' ) ); ?></div>
		</nav><!-- #nav-below -->
	<?php endif; ?>				

</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>