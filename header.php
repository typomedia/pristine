<?php
/**
 * @package WordPress
 * @subpackage Pristine
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title('- ', true, 'right'); ?><?php bloginfo('name'); ?></title>
<link rel="icon" type="image/x-icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
<?php if ( is_singular() && get_option('thread_comments') ) wp_enqueue_script('comment-reply'); ?>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
<![endif]-->
<!-- Google Analytics -->
<?php include_once("includes/analytics.php"); ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper">
	<header id="header" role="banner">

		<hgroup>
			<h1 id="sitename"><a href="<?php bloginfo('home'); ?>" title="<?php echo esc_attr( get_bloginfo('name', 'display') ); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
			<h4 id="subtitle"><?php bloginfo('description'); ?></h4>
		</hgroup>

		<nav id="topnav" role="navigation">
			<?php wp_nav_menu( array('theme_location' => 'primary') ); ?>
		</nav><!-- #topnav -->

	</header><!-- #header -->