﻿<?php
/**
 * @package WordPress
 * @subpackage Pristine
 */
?>

	<footer id="colophon" role="contentinfo">
		Copyright &copy; <?php echo date('Y');?> <?php bloginfo('name');?>. <?php _e('All rights reserved.', 'pristine'); ?>
		<a class="wordpress-icon" href="http://wordpress.org/" title="Semantic Personal Publishing Platform">Proudly powered by WordPress. </a>
		<a class="pristine-icon" href="https://github.com/typomedia/pristine"><?php echo get_current_theme(); ?> Theme</a><br />
	</footer>
	<!-- #colophon -->
</div><!-- #wrapper -->

<?php wp_footer(); ?>
</body>
</html>