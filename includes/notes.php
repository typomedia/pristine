<div class="wrap">
<div id="icon-themes" class="icon32"><br /></div>
<h2>Pristine 1.0</h2>
ein Template f&uuml;r <a href="http://www.wordpress.org/">WordPress.org</a> von Philipp Speck

<p><b>Nutzungshinweise</b></p>
<p>Das Anzeigen der Webseite mit unterschiedlichen Browsern kann zu minimalen Abweichungen f&uuml;hren. Das Template wurde in <i>Adobe Dreamweaver CS4 </i> programmiert und unter den Browsern <i>Mozilla Firefox</i>, <i>Microsoft Internet Exolorer</i>, <i>Safari</i> und <i>Opera</i> getestet. Bei der Erstellung wurde besonderer Wert auf eine suchmaschinefreundliche Gestaltung gelegt. Der Ansto&szlig; der Entwicklung lag in der Integration dynamischer Contents bzw. dem einfachen Aktualisieren der Inhalte. Die Vorgaben aus dem bestehenden Design waren themenabh&auml;ngige Banner und Seitenleisten.</p>

<p><b>Funktionen</b></p>
<p>Die  Felder <em>Auszug </em>und <em>Schlagw&ouml;rter</em> entsprechen den Metainformationen <em>Keywords</em> und <em>Description</em>. Der Tag <em>Description</em> gibt  vor, welcher Beschreibungstext in den Suchmaschinen erscheint. Wird keiner angeben, erstellt der Crawler einen Auszug aus dem Inhalt. Verkleinerte Bilder mit Link zum Original, werden beim Einf&uuml;gen mit einem Zoomeffekt versehen bzw. erhalten die CSS-Klasse <i>thickbox</i>. Alle Bilder werden au&szlig;erdem mit einem Schatten hinterlegt. Mit dem Einf&uuml;gen der CSS-Klasse <em>none</em> in der HTML-Ansicht kann dies unterbunden werden. <br />
Da &Uuml;berschriften ein wichtiges Kriterium bei der Suchmaschinenerfassung sind, sollten diese auch nur sinnvoll eingesetzt werden. Das selbe gilt f&uuml;r Hervorhebungen (fett, kursiv...) aller Art.</p>

<p><b>Einschr&auml;nkungen</b></p>
<ul class="ul-disc">
<li>Aufgrund der festen Breite von <strong>800 Pixel</strong> ist nur eine beschr&auml;nkte Anzahl an Hauptmen&uuml;punkten in der Navigationsleiste m&ouml;glich.</li>
<li> Bilder und Texte der Seitenleisten k&ouml;nnen  unter dem Punkt <em>Sidebar</em> im Adminbereich editiert werden.</li>
<li> Damit die Zuordung von Seiten und Seitenleisten (Sidebar) korrekt funktioniert, m&uuml;ssen sie gleich lauten.</li>
<li>Spezielle Artikelbilder (lokale Banner) sollten eine Breite von mindestens <strong>800 Pixel</strong> und eine H&ouml;he von <strong>230 Pixel</strong> haben </li>
<li>Die Seitenleiste mit dem Namen <em>Standard</em> wird immer dann angezeigt, wenn keine spezifische angelegt wurde und darf nicht umbenannt werden. </li>
</ul>
<p><strong>Warnungen</strong></p>
<p>Einige Funktionen wurden speziell f&uuml;r dieses Template entwickelt und sind in anderen Themes nicht verf&uuml;gbar. Wenn Sie zuk&uuml;nftig also ein anderes Theme verwenden wollen oder in Auftrag geben, sollten Sie dies im Hinterkopf behalten und den Programmierer auf diese speziellen Funktionen (in der <em>functions.php</em> des Templates) hinweisen. Dazu geh&ouml;ren vor allem:</p>
<ul class="ul-disc">
  <li>Bearbeitung der Seitenleisten</li>
  <li>Globale  und lokale Bannereinstellungen</li>
  <li>Zoomeffekt f&uuml;r verlinkte Bilder</li>
  <li>Schlagw&ouml;rter und Ausz&uuml;ge f&uuml;r Seiten</li>
  <li>Unterst&uuml;tzung von Meta-Description </li>
</ul>
<p><b>Versionshinweise</b></p>
<ul class="ul-disc">
<li>Version 2.0: kleine Anpassungen am Stylesheet </li>
<li>Version 1.9: Auswahl des Banners f&uuml;r Einzelseiten</li>
<li>Version 1.8: Globale Anpassung des Banners</li>
<li>Version 1.7: Verwendbarkeit von Tags und Excerpt f&uuml;r Seiten</li>
<li>Version 1.6: Bearbeitung der Seitenleisten aus dem Backend</li>
<li>Version 1.5: Meta Robots Tags f&uuml;r Artikel und  Seiten</li>
<li>Version 1.4: &uuml;berarbeiteter Seitentitel und Metadaten</li>
<li> Version 1.3: Quellcode optimiert, Schatten hinzugef&uuml;gt</li>
<li> Version 1.2: themenabh&auml;ngige Banner und Seitenleisten</li>
<li> Version 1.1: Thickbox Zoomeffekt f&uuml;r Bilder integriert</li>
<li> Version 1.0: Fertigstellung des Templates </li>
</ul>
<p>&nbsp;</p>
<hr/>
<p></p>
<p>Copyright &copy; <?php echo date('Y');?> <a href="http://www.destio.de" target="_blank">Designstudio, Philipp Speck</a>. Alle Rechte vorbehalten.</p>